# Valuefy


(Angular CLI: 6.0.8)
(Node: 10.16.1)
(OS: linux x64)
(Angular: 6.1.10)

(@angular-devkit/architect         0.6.8)
(@angular-devkit/build-angular     0.6.8)
(@angular-devkit/build-optimizer   0.6.8)
(@angular-devkit/core              0.6.8)
(@angular-devkit/schematics        0.6.8)
(@angular/cli                      6.0.8)
(@ngtools/webpack                  6.0.8)
(@schematics/angular               0.6.8)
(@schematics/update                0.6.8)
(rxjs                              6.0.0)
(typescript                        2.7.2)
(webpack                           4.8.3)

Just check the above dependencies. Then Clone the Project. Run Npm Install.Then follow the below steps
    


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
